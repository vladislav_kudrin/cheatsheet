package com.example.cheatsheet;

import androidx.appcompat.app.AppCompatActivity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;

/**
 * Runs an application's activity.
 *
 * @author DeltaZero & Vladislav
 * @version 1.1
 * @since 03/21/2021
 */
public class MainActivity extends AppCompatActivity {
    private Intent secondActivity;

    /**
     * Creates the application's activity.
     *
     * @param savedInstanceState previous saved data states.
     */
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        secondActivity = new Intent(this, SecondActivity.class);
        ListView generationsListView = findViewById(R.id.gensListView);
        generationsListView.setAdapter(new ArrayAdapter<>(this, R.layout.listview_text, getResources().getStringArray(R.array.generations_list)));
        generationsListView.setOnItemClickListener((parent, view, index, id) -> {
            secondActivity.putExtra("SELECTED_ITEM_INDEX", index);
            startActivity(secondActivity);
        });
    }
}