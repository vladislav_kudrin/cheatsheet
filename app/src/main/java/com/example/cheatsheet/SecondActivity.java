package com.example.cheatsheet;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;

/**
 * Runs an application's activity.
 *
 * @author Vladislav
 * @version 1.2
 * @since 03/21/2021
 */
public class SecondActivity extends AppCompatActivity {

    /**
     * Creates the application's activity.
     *
     * @param savedInstanceState previous saved data states.
     */
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);

        WebView webView = findViewById(R.id.webView);
        webView.setWebViewClient(new WebViewClient() {
            /**
             * Makes {@code view} visible if page content is loaded.
             *
             * @param view the initiated WebView.
             * @param url the page's url.
             */
            public void onPageFinished(WebView view, String url) {
                view.setVisibility(View.VISIBLE);
            }
        });
        webView.setOnLongClickListener(view -> true);
        webView.setHapticFeedbackEnabled(false);

        webView.loadUrl(getResources().getStringArray(R.array.generations_pages)[getIntent().getExtras().getInt("SELECTED_ITEM_INDEX")]);
    }
}